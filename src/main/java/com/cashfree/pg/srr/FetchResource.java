package com.cashfree.pg.srr;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


import org.eclipse.microprofile.rest.client.inject.RestClient;

import io.smallrye.mutiny.Uni;

@Path("/fetch")
public class FetchResource {

    @Inject
    @RestClient
    FetchService service;


    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<String> fetchSiteByName() {
        return service.getByUri();
    }
}