package com.cashfree.pg.srr;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import io.smallrye.mutiny.Uni;

@ApplicationScoped
@RegisterRestClient
@Path("/")
public interface FetchService {
    
    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<String> getByUri();


}
